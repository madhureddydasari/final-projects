import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DeveloperloginPage } from './developerlogin.page';

describe('DeveloperloginPage', () => {
  let component: DeveloperloginPage;
  let fixture: ComponentFixture<DeveloperloginPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeveloperloginPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DeveloperloginPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
