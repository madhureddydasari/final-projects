import { Component, OnInit } from '@angular/core';
import { ToastController, NavController, AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'app-developerlogin',
  templateUrl: './developerlogin.page.html',
  styleUrls: ['./developerlogin.page.scss'],
})
export class DeveloperloginPage implements OnInit {

  email : string= ""
  password : string = ""

  constructor(    private toastCtrl : ToastController,
    private navCtrl : NavController,
    public afAuth: AngularFireAuth,
    public alert : AlertController,
    public router :Router) { }

  ngOnInit() {
    this.afAuth.auth.onAuthStateChanged((user) => {
      if(user)
      {
        this.navCtrl.navigateRoot('/home');
      }
      else{
      
      }
    })
  }

  async loginaction()
  {
    const { email,password } = this

    try{
      const res= await this.afAuth.auth.signInWithEmailAndPassword(email+'@developermail.com',password)
      .then(()=>{
        this.showToast("user successfully logged in");
        this.navCtrl.navigateRoot('/home');
      })

    }
    catch(err){
      // if(err.code == "auth/invalid-email")
      // if(err.code == "auth/invalid-email")
      // {
        console.log("username and password doesnot match")
        // this.showAlert("Error!","Invalid Username and Password")
        this.showAlert("Error",err.message)
      // }
      
      // else{
      //   console.log(err.message)
      // this.router.navigate(['/index/login'])
      // this.showAlert("Error!","Invalid Username and Password")
      // }
      
    }

  }


  async showAlert(header :string , message :string)
  {
    const alerrt = await this.alert.create({
      header,
      message,
      buttons: ["OK"]
    })
    await alerrt.present()
  }
  showToast(message :string){
    this.toastCtrl.create({
      message,
      duration:2000,
    }).then((toast) => toast.present())
  }
}
