import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DoctregisterPage } from './doctregister.page';

const routes: Routes = [
  {
    path: '',
    component: DoctregisterPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DoctregisterPageRoutingModule {}
