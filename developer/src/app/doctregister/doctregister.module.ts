import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DoctregisterPageRoutingModule } from './doctregister-routing.module';

import { DoctregisterPage } from './doctregister.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DoctregisterPageRoutingModule
  ],
  declarations: [DoctregisterPage]
})
export class DoctregisterPageModule {}
