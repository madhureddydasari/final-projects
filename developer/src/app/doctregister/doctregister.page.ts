import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';
import {AlertController, NavController} from '@ionic/angular'
import { Router } from "@angular/router";
import { Doctorstruct } from "../modal/Docotor";
// import { DoctorLoginstruct } from "src/app/modal/doctorLogin";
import { FirebaseService } from "../services/firebase.service";

// import { DoctorloginService } from "src/app/services1/doctorlogin.service";

@Component({
  selector: 'app-doctregister',
  templateUrl: './doctregister.page.html',
  styleUrls: ['./doctregister.page.scss'],
})
export class DoctregisterPage implements OnInit {

  username :string=""
  password :string=""
  cpassword : string=""
  name : string=""


  tempid:string;

  doctor : Doctorstruct = {
    name:'',
    // docId : '',
    description:'',
    department : '',
    phone: '',
    specialization: '',
    consultationFee : '',
    timings:'',
    facilities:'',
    toInclude:true,
    // docUID?:"", no need to declare u can access 
    emailId :"" ,
    password : "",
    
  };
  constructor(
    // private doctorloginService:Doctorstruct,
    private fbservice : FirebaseService,
    public afAuth: AngularFireAuth,
    public alert :AlertController,
    public router : Router,
    private navCtrl : NavController,
  ) { }

  ngOnInit() {
  }

  async registeraction() 
  {
    const {username ,password ,cpassword}=this
    if(username !== "")
    {
      if(password !== cpassword){
        this.showAlert("Error!","passwords doesnt match")
        return  console.log("passwords doesnt match")
      }

      // try {
      //       await this.afAuth.auth.createUserWithEmailAndPassword(username+'@hmsmail.com' , password)
      //       .then((user)=>{   
      //         this.doctor.createdAt= new Date().getTime()
      //         this.doctor.toInclude=true
      //         this.doctor.doctorExist=false
      //         this.doctor.createdOn = new Date()
      //         this.doctor.doctuserUID=user.user.uid
      //         this.doctor.emailId=user.user.email
      //         this.doctor.password=this.password
      //         this.doctor.name=this.name
      //         this.fbservice.addDoctor(this.doctor)
      //       })
      //       .catch((err) => {
      //           console.log(err) 
      //           console.dir(err)
      //           this.showAlert("Error",err.message)
      //         })
      //         // console.log("iafter implementing")
 
      //   // this.showAlert("Success","Registered Successfully")
      //   // this.afAuth.auth.signOut
      // }

       try {
        await this.afAuth.auth.createUserWithEmailAndPassword(username+'@hmsmail.com' , password);
          const useruid = this.afAuth.auth.currentUser;
          this.doctor.createdAt = new Date().getTime();
          this.doctor.toInclude=true
          this.doctor.doctorExist= false
          this.doctor.createdOn =  new Date()
          this.doctor.doctuserUID= useruid.uid
          this.doctor.emailId= useruid.email
          this.doctor.password= this.password
          this.doctor.name= this.name
           await this.fbservice.addDoctor(this.doctor)
      
        .catch(async (err) => {
            console.log(err) ;
            console.dir(err);

            await this.showAlert("Error",err.message)
          })
          // console.log("iafter implementing")

    // this.showAlert("Success","Registered Successfully")
    // this.afAuth.auth.signOut
  }

      catch(error){
        console.dir(error)
        await this.showAlert("Error",error.message)
      }
    }
    else {
      await this.showAlert("error","enter valid Email address")
    }

  }

  async showAlert(header :string , message :string)
  {
    const alert = await this.alert.create({
      header,
      message,
      buttons: ["OK"]
    })
    await alert.present()
  }

}
