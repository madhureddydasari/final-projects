export interface Doctorstruct {
        doctorExist ?:boolean;
        id?: any;
        name: string;
        description : string;
        department : string;
        phone : string;
        specialization: string;
        consultationFee : string;
        timings:string;
        facilities: string;
        createdAt? :any;
        createdOn? :any;
        lastModified? :any ;
        docId ?: any;
        doctuserUID?:string;
        emailId ?: string;
        password ?: string;
        toInclude ?: boolean; 
        startTime?:any;
        endTime?:any;
        timeSlots?: any[];
        interval?:number;
                 // given in doctregister.ts "yes" by default while adding for first time
    
    } 


// export interface bookingstruct {
//     id?: any;
//     userID : string ;
//     doctorid : string ;
//     doctorname : string ;
//     status : string ;
// }