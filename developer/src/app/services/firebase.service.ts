import { Injectable } from '@angular/core';
import { Observable } from "rxjs";
import * as firebase from "firebase";
import { Doctorstruct, } from "../modal/Docotor";
import { map, take } from "rxjs/operators";
import { AngularFirestoreCollection, AngularFirestore, DocumentReference } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { ToastController, NavController } from '@ionic/angular';


@Injectable({
  providedIn: 'root'
})
export class FirebaseService {

  tempId :string;
  private doctorsdb: Observable<Doctorstruct[]>

  private doctorsdb1: Observable<Doctorstruct[]>
  private doctorsdb2: Observable<Doctorstruct[]>

  private doctorCollection: AngularFirestoreCollection<Doctorstruct>;

  private doctorCollection1: AngularFirestoreCollection<Doctorstruct>;
  private doctorCollection2: AngularFirestoreCollection<Doctorstruct>;

  // private bookingCollection: AngularFirestoreCollection<bookingstruct>;


  constructor( private navCtrl : NavController,private toastCtrl : ToastController,private afs : AngularFirestore ,private Auth : AngularFireAuth) 
  {
    this.doctorCollection=this.afs.collection<Doctorstruct>('DoctorsList');

    this.doctorCollection1=this.afs.collection<Doctorstruct>('DoctorsList',ref => ref.where("doctorExist","==",true));
    this.doctorCollection2=this.afs.collection<Doctorstruct>('DoctorsList',ref => ref.where("doctorExist","==",false));

   
    this.doctorsdb = this.doctorCollection.snapshotChanges().pipe(
      map(actions =>{
        return actions.map(a=> {
           const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    )

    this.doctorsdb1 = this.doctorCollection1.snapshotChanges().pipe(
      map(actions =>{
        return actions.map(a=> {
           const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    )

    this.doctorsdb2 = this.doctorCollection2.snapshotChanges().pipe(
      map(actions =>{
        return actions.map(a=> {
           const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    )


   } //end of Constructor

 
   getDoctors() : Observable<Doctorstruct[]>{
     return this.doctorsdb;
   }

   getActiveDoctors() : Observable<Doctorstruct[]>{
    return this.doctorsdb1;
  }  
  getInActiveDoctors() : Observable<Doctorstruct[]>{
    return this.doctorsdb2;
  }

   

   getDoctor(idd: string) : Observable<Doctorstruct>{
     return this.doctorCollection.doc<Doctorstruct>(idd).valueChanges().pipe(
       take(1),
       map(note=> {
         note.id=idd;
         return note;
       })
     );
   }

   

    async addDoctor(doctor: Doctorstruct) 
    {
      return await this.doctorCollection.add(doctor).then(ref => {
        this.tempId=ref.id;
        ref.set({ docId : ref.id }, { merge: true }) })// if we dont use merge remaining fields will vanish
        .then(() => this.showToast("doctor successfully added but hidden")  ) 
        .then(async () => {await this.navCtrl.navigateForward(`/update-doctor/${this.tempId}`) })  
        // .then(() => { this.Auth.auth.signOut() })
         
    } 

    enableDisableDoctor(doctor :Doctorstruct)
    {
      return this.doctorCollection.doc(doctor.id).update({doctorExist : doctor.doctorExist})
    }


  
  //  insertfield(doctor: Doctorstruct ,_fname:string,fvalue:string):Promise<void> {
  //    return this.doctorCollection.doc(doctor.id).update( { "_fname" : _fname });
  //  }

   updateDoctor(doctor: Doctorstruct) : Promise<void>{
    return this.doctorCollection.doc(doctor.id).update({
                                                  lastModified :new Date().getTime(),

                                                  startTime: doctor.startTime,
                                                  endTime: doctor.endTime,
                                                  interval : doctor.interval,
                                                  timeSlots : doctor.timeSlots,
                                                        name : doctor.name ,
                                                        department :doctor.department,
                                                        description :doctor.description,
                                                          phone : doctor.phone,
                                                          specialization : doctor.specialization,
                                                            consultationFee : doctor.consultationFee,
                                                            doctorExist : doctor.doctorExist
                                                                // timings : doctor.timings , facilities : doctor.facilities, 
                                                              }).then(()=>{this.showToast("doctor details successfully updated")})
                                                      }




  // deleteDoctor(id:string): Promise<void> {
  //   return this.doctorCollection.doc(id).delete();
  // }

  async showToast(message : string){
    const toast =await this.toastCtrl.create({
      message ,
      duration : 2000
    })
    toast.present()
  }

}
 