import { Injectable } from '@angular/core';
import { concat } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DivideTimeSlotService {

  // startTime:string = "11:00:00";
  // endTime = "12:00:00";
  times_ara: any[];

  constructor() { }

  ngOnInit() {
   
    // let interval = 30;
  
  console.log(this.times_ara)
  }

  divideTimeSlots(startTime :string , endTime:string ,interval:number)
  {
    let start_time = this.parseTime(startTime);
    let end_time = this.parseTime(endTime);
    
    return this.times_ara = this.calculate_time_slot( start_time, end_time , interval);
  }

  parseTime(s) {
    var c = s.split(':');
    return parseInt(c[0]) * 60 + parseInt(c[1]);
  }

  convertHours(mins)
  {
    let hour = Math.floor(mins/60);
    let mins2 = mins%60;
    var converted = this.pad(hour, 2)+':'+this.pad(mins2, 2);
    return converted;
  }

  pad(str, max) {
    str = str.toString();
    return str.length < max ? this.pad("0" + str, max) : str;
  }

  calculate_time_slot(start_time, end_time, interval){
      // var i, formatted_time;
    let time_slots = new Array();
      for(let i=start_time,j=1; i<=end_time-interval; i = i+interval,j++){
      let slotTime1 = this.convertHours(i);
      let slotTime2 = this.convertHours(i+interval);
      let slotTime = slotTime1+" - "+slotTime2
      let slotId =1000+j
      time_slots.push({slotId,slotTime});
    }
    return time_slots;
  }
}



