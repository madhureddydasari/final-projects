import { Component, OnInit } from '@angular/core';
import { Doctorstruct } from '../modal/Docotor';
import { ActivatedRoute, Router } from '@angular/router';
import { FirebaseService } from '../services/firebase.service';
import { DivideTimeSlotService } from '../services/timeslots/divide-time-slot.service';
import { AlertController } from '@ionic/angular';
 
@Component({
  selector: 'app-update-doctor',
  templateUrl: './update-doctor.page.html',
  styleUrls: ['./update-doctor.page.scss'],
})
export class UpdateDoctorPage implements OnInit {


  doctor : Doctorstruct ={
    // docId:'',
    name:'',
    department:'',
    description:'',
    phone: '',
    specialization: '',
    consultationFee : '',
    timings:'',
    facilities:'',
    startTime:'',
    endTime:'',
    interval:null,
  }

  constructor(private alertCtrl : AlertController,private fbtimeservice : DivideTimeSlotService,private activatedRoute : ActivatedRoute , private fbservice : FirebaseService , private router : Router) { }

  ngOnInit() {
    const id= this.activatedRoute.snapshot.paramMap.get('id');
    if(id)
    {
      this.fbservice.getDoctor(id).subscribe(docData => {
        this.doctor = docData 
      });
    }
  }
  
  // change(){
  //   this.fbservice.enableDisableDoctor(this.doctor,this.enableOrDisable)
  // }

  updateDoctor()
  {
    try
    {  
        try{
          if(this.doctor.startTime && this.doctor.endTime && this.doctor.interval)
          {
            this.doctor.timeSlots = this.fbtimeservice.divideTimeSlots(this.doctor.startTime , this.doctor.endTime,this.doctor.interval)
          }
        }
        catch(err)
        {
          this.showAlert(err.message)
        }



        // this.fbservice.enableDisableDoctor(this.doctor,this.enableOrDisable) 
        // no need of a SEPARATE piece of code as we merged that with the doctor struct and directly updating
        this.fbservice.updateDoctor(this.doctor).then(()=> {
          this.router.navigateByUrl(`/view-doctor/${this.doctor.id}`)
        });
    }
  catch(err)
  {
    if(err.code = "invalid-argument"){
      this.showAlert("please fill StartTime , EndTime and interval to generate TimeSlots ")

    }
    else{
      this.showAlert("error code is : "+err.code)
    }
  }

  }

  async showAlert(message : string)
  {
    const alert = await this.alertCtrl.create ({
      header:'Warning',
      message ,
      buttons:["OK"]
    })
    alert.present()
  }

}
