import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule)
  },
  // {
  //   path: 'home/:id',
  //   loadChildren: () => import('./pages/home/home.module').then( m => m.HomePageModule)
  // },
  // {
  //   path: 'history/:id',
  //   loadChildren: () => import('./pages/history/history.module').then( m => m.HistoryPageModule)
  // },
  {
    path: 'firstloadpage/:id1',
    loadChildren: () => import('./pages/firstloadpage/firstloadpage.module').then( m => m.FirstloadpagePageModule)
  },
  {
    path : 'history/:id',
    children : [
      {
        path: '',
        loadChildren: () => import('./pages/history/history.module').then( m => m.HistoryPageModule)
      },
    ] 
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
