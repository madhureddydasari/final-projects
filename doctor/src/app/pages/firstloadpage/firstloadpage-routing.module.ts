import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FirstloadpagePage } from './firstloadpage.page';

const routes: Routes = [
  {
    path: '',
    component: FirstloadpagePage,
  // },
    children:[
      {
        path: '',
        children :[
          {
            path:'home/:id',
            children:
            [
              {
                path : '',
                loadChildren: () => import('../home/home.module').then( m => m.HomePageModule)
              },

              {
                path : 'history/:id',
                children : [
                  {
                    path: '',
                    loadChildren: () => import('../history/history.module').then( m => m.HistoryPageModule)
                  },
                ] 
              }

          ]
          },


      ]
      },
    ]
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FirstloadpagePageRoutingModule {}
