import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FirstloadpagePageRoutingModule } from './firstloadpage-routing.module';

import { FirstloadpagePage } from './firstloadpage.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FirstloadpagePageRoutingModule
  ],
  declarations: [FirstloadpagePage]
})
export class FirstloadpagePageModule {}
