import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FirstloadpagePage } from './firstloadpage.page';

describe('FirstloadpagePage', () => {
  let component: FirstloadpagePage;
  let fixture: ComponentFixture<FirstloadpagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FirstloadpagePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FirstloadpagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
