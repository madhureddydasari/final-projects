import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { bookingstruct } from 'src/app/modal/booking';
import { BookingService } from 'src/app/services/booking.service';

@Component({
  selector: 'app-history',
  templateUrl: './history.page.html',
  styleUrls: ['./history.page.scss'],
})
export class HistoryPage implements OnInit {

  bookingsdb1 : Observable<bookingstruct[]>

  constructor(private fbBookingService : BookingService,private activatedRoute : ActivatedRoute , private navCtrl : NavController) 
  {
    let id=this.activatedRoute.snapshot.paramMap.get('id')
    if(id)
    {
      this.bookingsdb1=this.fbBookingService.getBookingsInParticular(id)
    }
  }

  ngOnInit() {
  }


}
