import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { DoctorService } from "src/app/services/doctor.service";
import { Doctorstruct } from 'src/app/modal/doctor';
import { NavController, AlertController } from '@ionic/angular';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
 

  doctorsdb : Observable<Doctorstruct[]>
  
  // singledoctor: Doctorstruct;

  // onedoctordb: Doctorstruct ={
  //   name: "",
  //   description : "",
  //   department : "",
  //   phone : "",
  //   specialization: "",
  //   consultationFee : "",
  //   timings:"",
  //   facilities: "",
  // }
  constructor(private router : Router,private alertCtrl :AlertController , private Aauth : AngularFireAuth,private navCtrl : NavController,private fbDoctorService:DoctorService,private activatedRoute : ActivatedRoute) 
  {

  }
  
  ngOnInit() {
    let id = this.activatedRoute.snapshot.paramMap.get('id')
    if(id)
    {
      this.doctorsdb = this.fbDoctorService.getDoctorsInParticular(id)
      // this.doctorsdb.subscribe((result) => { this.singledoctor = result[0] })
    }

  }

  // getDoctorAppointments(id :string)
  // {
  //   this.navCtrl.navigateRoot(`/history/3rGVNCKdssj8cCpIZb7N`)
  //   // this.router.navigate(`history/${id}`)
  // }

  logout()
  {
    this.alertCtrl.create({
      message:"Are u sure , u want to logout",
      buttons : [{text :"cancel"},
                { text : "ok",
                  handler : ()=> {
                    this.Aauth.auth.signOut().then(()=>{
                      this.navCtrl.navigateRoot("login")
                    })
                  }
              }]
    }).then((alert)=>{ alert.present() })
    

  }
  changetoInclude(doctor : Doctorstruct)
  {
    this.fbDoctorService.updateDoctor(doctor)

  }
}