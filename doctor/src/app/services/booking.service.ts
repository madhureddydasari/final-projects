

import { Injectable } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestore , DocumentReference } from '@angular/fire/firestore';
import { bookingstruct } from 'src/app/modal/booking'
import { Observable } from 'rxjs';
import {  map, take} from 'rxjs/operators';
import { AngularFireAuth } from '@angular/fire/auth';
// import { AngularFireDatabase } from "@angular/fire/database";
// import { promise, $ } from 'protractor';
import { ToastController, NavController } from '@ionic/angular';


@Injectable({
  providedIn: 'root'
})
 
export class BookingService {
  
  bookingsdb : Observable<bookingstruct[]>
  bookingCollection : AngularFirestoreCollection<bookingstruct>

  constructor(private navCtrl : NavController,private toastCtrl : ToastController, private afs : AngularFirestore , private Aauth : AngularFireAuth) 
  { 
  }


  getBookingsInParticular(currentDoctorID) : Observable<bookingstruct[]>
  {         
        this.bookingCollection = this.afs.collection<bookingstruct>('BookingsList' , ref => ref.where('doctorID','==',currentDoctorID)) ;
        this.bookingsdb = this.bookingCollection
        .snapshotChanges().pipe(
          map(actions =>{
            return actions.map(a=> {
              const data = a.payload.doc.data();
              const id = a.payload.doc.id;
              return { id, ...data };
            });
          })
        )
        return this.bookingsdb;
  }

  // getbooking(id: string) : Observable<bookingstruct>{
  //   return this.bookingCollection.doc<bookingstruct>(id).valueChanges().pipe(
  //     take(1),
  //     map(firestoreDocumentValue => {
  //       firestoreDocumentValue.id = id;
  //       return firestoreDocumentValue;
  //     })
  //   );
  // }


  // bookDoctor(book : bookingstruct) 
  // {
  //         let uid = this.Aauth.auth.currentUser.uid ;
  //         book.userUID = uid ;
  //         return this.bookingCollection.add(book)
  //         .then(ref => {
  //           ref.set({ bookingId : ref.id},{merge : true})
  //           // merege says that add field bookingId and everything else is same as before
  //           .then(() => {
  //             // console.log("add aextra field also")
  //             this.toastCtrl.create({
  //               message : "booking id is :"+ ref.id,
  //               duration : 2000
  //             }).then((toast) => {
  //               toast.present()
  //             })
  //           })
  //         })
    
  //   }
}
 