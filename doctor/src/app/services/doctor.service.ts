import { Injectable } from '@angular/core';
import { Observable } from "rxjs";
import { Doctorstruct } from "../modal/doctor";
import { map, take } from "rxjs/operators";
import { AngularFirestoreCollection, AngularFirestore, DocumentReference } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { ToastController, NavController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class DoctorService {

  private doctorsdb: Observable<Doctorstruct[]>
  private doctorCollection: AngularFirestoreCollection<Doctorstruct>;

  constructor(private afs : AngularFirestore) 
  {


  }

  getDoctorsInParticular(activatedId :string)  
  {
    this.doctorCollection=this.afs.collection<Doctorstruct>('DoctorsList',ref => ref.where("doctuserUID","==",activatedId) );
    this.doctorsdb = this.doctorCollection.snapshotChanges().pipe(
      map(actions =>{
        return actions.map(a=> {
           const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    )
    return this.doctorsdb;
  }

  updateDoctor(doctor : Doctorstruct)
  {
    this.doctorCollection.doc(doctor.id).set({toInclude :!doctor.toInclude},{merge :true})
  }
}
