export interface Doctorstruct {
    id?: any;
    name: string;
    department:'';
    phone : string;
    description : string;
    specialization: string;
    consultationFee : string;
    timings:string;
    facilities: string;
    createdAt :any;
    timeSlots?: any[];
 
}

export interface bookingstruct {
    id? : any;
    userPatientListDocId? : any;
    userFullName? : any;
    bookingId? : string;
    doctorID? : string ;
    doctorName? : string;
    bookingDate? : any;
    userUID? : string;
    status? : string;
    bookedSlot?:{ slotId: any; slotTime: any; };
}

// export interface userStruct {
//     id?:any; // this document id 
//     emailId : string; //no edits
//     userUID : string;   //no edits
//     createdON : any;
//     userfullname : string;
//     userImageUrl : string;
// }