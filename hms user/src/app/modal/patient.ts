export interface userStruct {
    limit?:number;
    lastBookingDate?:any;
    userFullName: any;
    id?:any; // this document id 
    emailId : string; //no edits
    userUID : string;   //no edits
    createdON : any;
    userImageUrl : string;
}