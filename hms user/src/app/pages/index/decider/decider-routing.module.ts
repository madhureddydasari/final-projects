import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DeciderPage } from './decider.page';

const routes: Routes = [
  {
    path: '',
    component: DeciderPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DeciderPageRoutingModule {}
