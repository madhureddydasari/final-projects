import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DeciderPageRoutingModule } from './decider-routing.module';

import { DeciderPage } from './decider.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DeciderPageRoutingModule
  ],
  declarations: [DeciderPage]
})
export class DeciderPageModule {}
