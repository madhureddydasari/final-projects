import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';
import {AlertController, NavController, ToastController} from '@ionic/angular'
import { Router } from "@angular/router";
import { userStruct } from 'src/app/modal/patient';
// import { FirebasefirestoreService } from "src/app/services/firebasefirestore.service";
import { PatientService } from 'src/app/services/patientservice/patient.service';


@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {
  userFullName:string=""
  email :string=""
  password :string=""
  cpassword : string=""

  patientdata : userStruct = {
    lastBookingDate:"",
    limit:0,
    userFullName :"",
    userUID : "",
    emailId:"",
    userImageUrl : "",
    createdON : "",
}
// id:string ;
  constructor(
    private toastCtrl : ToastController ,
    private patientService:PatientService,
    public afAuth: AngularFireAuth,
    public alert :AlertController,
    public router : Router,
    private navCtrl : NavController,
  ) { }


  ngOnInit() {
  }

  registeraction() 
  {
    const {email ,password ,cpassword}=this
    if(email !== "")
    {
      if(password !== cpassword){
        this.showAlert("Error!","passwords doesnt match")
        return  console.log("passwords doesnt match")
      }

      try {
            this.afAuth.auth.createUserWithEmailAndPassword(email+'@email.com' , password)
            .then((user)=>{  
              this.patientdata.lastBookingDate=new Date();
              this.patientdata.limit=0;
              this.patientdata.userFullName = this.userFullName
              this.patientdata.createdON = new Date();
              this.patientdata.userUID=user.user.uid
              this.patientdata.emailId=user.user.email
              this.patientService.addPatient(this.patientdata)
              // .then((res)=>{console.log("usert details are follows"+res) })
            })
            // .then(() => {
            //   this.showToast("user created successfully")
            //   this.navCtrl.navigateRoot("/index/login"); 
            // })
            .catch((err) => {
                // console.log(err) 
                this.showAlert("Warning",err.message)
              })
              // console.log("iafter implementing")
 
        // this.showAlert("Success","Registered Successfully")
        // this.afAuth.auth.signOut
      }
      catch(error){
        console.dir(error)
        this.showAlert("Error",error.message)
      }
    }
    else {
      this.showAlert("error","enter valid Email address")
    }

  }

  async showAlert(header :string , message :string)
  {
    const alert = await this.alert.create({
      header,
      message,
      buttons: ["OK"]
    })
    await alert.present()
  }


  showToast(message :string){
    this.toastCtrl.create({
      message,
      duration:2000,
    }).then((toast) => toast.present())
  }

}
 