import { Component, OnInit } from '@angular/core';
import { bookingstruct } from 'src/app/modal/Doctor';
import { Observable } from 'rxjs';
import { AngularFireAuth } from '@angular/fire/auth';
// import { FirebasefirestoreService } from 'src/app/services/firebasefirestore.service';
import {  DoctorService } from "src/app/services/firebasedoctor/doctor.service";
import { BookingService } from "src/app/services/bookingservice/booking.service";
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-history',
  templateUrl: './history.page.html',
  styleUrls: ['./history.page.scss'],
})
export class HistoryPage implements OnInit {
  book : bookingstruct={
    doctorID : "" ,
    bookingId:"",
    doctorName : "",
    bookingDate : "",
    userUID : "",
    status:'',
  
  } 

  bookingsdb: Observable<bookingstruct[]>;

  userId :string ;

  constructor(private navCtrl : NavController,private Aauth : AngularFireAuth , private fbservicedoctor : DoctorService , private fbservicebooking : BookingService ) 
  {
    this.userId = this.Aauth.auth.currentUser.uid
    this.bookingsdb = this.fbservicebooking.getBookings(this.userId);
  }  

  ngOnInit() {
    console.log(this.userId)
    this.userId = this.Aauth.auth.currentUser.uid
    this.bookingsdb = this.fbservicebooking.getBookings(this.userId);
  }

  goback(){ 
    this.navCtrl.navigateRoot('/sidemenu/sidehome')
  }
 

}
