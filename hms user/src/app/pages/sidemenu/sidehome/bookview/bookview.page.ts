import { Component, OnInit  } from '@angular/core';
// import { FirebasefirestoreService } from 'src/app/services/firebasefirestore.service';
import { DoctorService } from "src/app/services/firebasedoctor/doctor.service";
import {  BookingService } from "src/app/services/bookingservice/booking.service";
import { ActivatedRoute , Router} from '@angular/router';
import { Doctorstruct, bookingstruct } from 'src/app/modal/Doctor';
import { userStruct } from "src/app/modal/patient";
import { AlertController, NavController, ToastController, Platform } from '@ionic/angular';
import { Observable } from 'rxjs';
import { AngularFireAuth } from '@angular/fire/auth';
import { PatientService } from 'src/app/services/patientservice/patient.service';
// import { Calendar } from '@ionic-native/calendar';
// import { not } from '@angular/compiler/src/output/output_ast';

// this is API which is to use calender options 
// import { Hammer } from "hammerjs";
import { CalendarComponentOptions } from 'ion2-calendar'
// import 'hammerjs/hammer'

@Component({
  selector: 'app-bookview',
  templateUrl: './bookview.page.html',
  styleUrls: ['./bookview.page.scss'],
})
export class BookviewPage implements OnInit {

  patient : userStruct ={
    userFullName:"",
    emailId : "", //no edits
    userUID : "",   //no edits
    createdON :"",
    userImageUrl : "",
  }

  doctor : Doctorstruct ={
    name:'',
    description:'',
    department:'',
    phone: '',
    specialization: '',
    consultationFee : '',
    timings:'',
    facilities:'',
    createdAt: ''
  }
  book : bookingstruct={
    doctorID : "" ,
    bookingId:"", 
    doctorName : "",
    bookingDate : "",
    userUID : "",
    status:'',
    userPatientListDocId : "",
    userFullName:''
    
  }   
 
  
 patientsdb:Observable<userStruct[]>
//  patientsdb1:Observable<userStruct>
  bookingsdb: Observable<bookingstruct[]>;
  // patientsdb1: userStruct;
  // patientsonedb: userStruct;
  // private calendar:Calendar,
  date: string;
  type: 'string';
  dateMulti: string[];
  dateRange: { from: string; to: string; };
  
counter:boolean;
  constructor( private plt:Platform,private toastCtrl : ToastController,private Aauth : AngularFireAuth,private alertCtrl : AlertController ,private fbservicedoctor : DoctorService ,private fbservicepatient : PatientService, private fbservicebooking : BookingService , private router : Router,private activatedRoute : ActivatedRoute) 
  {
    this.counter=false;

    // this.plt.ready().then(() => {
    //   this.calendar.listCalendars().then(data=>{
    //    this.calendar = data; 
    //   });
    // });
   }

 
      ngOnInit() {

        const id= this.activatedRoute.snapshot.paramMap.get('id')    
        if(id)
        {
          this.fbservicedoctor.getDoctor(id).subscribe(docData => {
            this.doctor=docData;
          });
        }

        let userUId = this.Aauth.auth.currentUser.uid;
        this.patientsdb = this.fbservicepatient.getPatients(userUId)
        console.log(userUId)
        // this returns all Bookings the particular user 
        // this.bookingsdb = this.fbservicebooking.getBookings(userUId);

        this.bookingsdb = this.fbservicebooking.getAllBookingsInParticular(userUId,id);




  } 
 
  addBookingShow(patient : userStruct ,slot: { slotId: any; slotTime: any; })
  { 
    console.log(`slotId is ${slot.slotId} and slotTime=${slot.slotTime}`)
    this.alertCtrl.create({
      message : "do u wanna book an appointment",
      buttons : [
        {
          text : 'cancel',
          handler:()=> {
            // console.log("")
          }
        },
        {
          text : 'ok',
          handler : () => {
            this.addBooking(patient,slot)
            // console.log("booking added")
                  // here we commented this bcz the above console log message is displayed
                  //  when the method is called  " DOESNT MATTER ITS RESULT " , IF it is successful or not
                  //  the result shoud be displayed in the method itself if successfull using "then()" promise 
                  // this just says that this method is called 
                  // -----------------DONE--------------but remember its crucial
                  
          }
        }]
    }).then((alert) =>{  
      alert.present();
    })
  }
 
  addBooking(patientInfo : userStruct,slot: { slotId: any; slotTime: any; })
  {
                
                // let uid=this.Aauth.auth.currentUser.uid
                // this.patientsdb=this.fbservicepatient.getPatients1(uid)
                // this.patientsdb.subscribe((patientsdb)=>{ this.patientsdb1=patientsdb[0] })

                // console.log(this.patientsdb1)
                // this.book.userFullName = this.patien
                // this.book.userPatientListId = this.patientsdb1.id,

                // if(patient.lastBookingDate == "")
                // {
                  if(patientInfo.lastBookingDate)
                  {
                    console.log("last booking date is"+patientInfo.lastBookingDate)
            //  as the data type of lastBookingDate is Date by default , no need to convert to "Date" to data type of Date
            // if it is of type "any" it needs to be converted to "Date" format , so we append ".toDate()"
                    let x=patientInfo.lastBookingDate.toDate().toLocaleDateString()

                    console.log("last booking date is"+x)

                    let y=new Date().toLocaleDateString()
                    console.log("last booking date is"+y)


                    if(x==y && patientInfo.limit>=0)
                    {
                      patientInfo.limit = patientInfo.limit + 1;
                      this.fbservicepatient.updatePatient1(patientInfo);
                      this.book.bookedSlot = slot

                      this.book.userFullName = patientInfo.userFullName;
                      this.book.userPatientListDocId = patientInfo.id;

                      this.book.doctorID = this.doctor.id,
                      this.book.doctorName = this.doctor.name,
                      this.book.status = "unvisited",
                      this.book.bookingDate =new Date().getTime(),
                      this.fbservicebooking.bookDoctor(this.book , patientInfo)
                    }
                    else if(x!=y)
                    {
                      patientInfo.lastBookingDate = new Date()
                      patientInfo.limit=1
                      this.fbservicepatient.updatePatient1(patientInfo)
                      
                      this.book.userFullName = patientInfo.userFullName;
                      this.book.userPatientListDocId = patientInfo.id;
                      this.book.bookedSlot = slot


                      this.book.doctorID = this.doctor.id,
                      this.book.doctorName = this.doctor.name,
                      this.book.status = "unvisited",
                      this.book.bookingDate =new Date().getTime(),
                      this.fbservicebooking.bookDoctor(this.book , patientInfo)
                    }
                    else{
                      this.alertCtrl.create({
                        message : " sry u have reached maximum limits for today to book an appointment",
                        buttons : ["ok"]      
                      }).then((alert)=>{ alert.present() })
                    }
                  }

                  else
                  {
                    // patientInfo.lastBookingDate = new Date()
                    // patientInfo.limit=1

                    let lastBookingDate = new Date()
                    let limit=1

                    // console.log("y is last booking date : "+patient.lastBookingDate)         
                    // let x=patient.lastBookingDate.toLocaleDateString()
                    // let y=new Date().toLocaleDateString()
                    // console.log("y is last booking date : "+y)  
                    this.fbservicepatient.updatePatient2(patientInfo , lastBookingDate , limit)
                    this.book.userFullName = patientInfo.userFullName;
                    this.book.userPatientListDocId = patientInfo.id;
                    this.book.bookedSlot = slot


                    this.book.doctorID = this.doctor.id,
                    this.book.doctorName = this.doctor.name,
                    this.book.status = "unvisited",
                    this.book.bookingDate =new Date().getTime(),
                    this.fbservicebooking.bookDoctor(this.book , patientInfo)       
                  }
                

            
                  
                // }
              
  }
   
  onChange($event: any) {
    console.log("event changed :"+$event);
  }

  showSlotss(x:boolean)
  {
    this.counter=x;
  }
  
  // optionsMulti: CalendarComponentOptions = {
  //   pickMode: 'multi'
  // };

  // optionsRange: CalendarComponentOptions = {
  //   pickMode: 'range'
  // };


  }

