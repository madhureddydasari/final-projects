import { NgModule } from '@angular/core';
import { Routes, RouterModule, ChildrenOutletContexts } from '@angular/router';

import { SidehomePage } from './sidehome.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: SidehomePage,
    children :[
      {
        path: 'home',
        children :[
          {
            path:'',
            loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
          },
          {
            path: 'homeview/:id',
            children:[
              {
                path:'',
                loadChildren: () => import('./homeview/homeview.module').then( m => m.HomeviewPageModule)
              },
              {
                path: 'bookview/:id',
                children : [
                  {
                    path:'',
                    loadChildren: () => import('./bookview/bookview.module').then( m => m.BookviewPageModule)

                  }
                ]
                // loadChildren: () => import('./bookview/bookview.module').then( m => m.BookviewPageModule)
              },
            ]
          }, 
        ] 
      },
      {
          path: 'notifications',
          children :[
            {
              path:'',
              loadChildren: () => import('./notifications/notifications.module').then( m => m.NotificationsPageModule)
            }
          ]
      },
      {
          path: 'feed',
          children:[
            {
              path :'',
              loadChildren: () => import('./feed/feed.module').then( m => m.FeedPageModule)
            }
          ]
      },

    
    ]
  },

  {
    path:'',
    redirectTo:'tabs/home',
    pathMatch:'full'    
  },


 
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SidehomePageRoutingModule {}



// const routes: Routes = [
//   {
//     path: 'tabs',
//     component: SidehomePage,
//     children :[
//       {
//         path: 'home',
//         children :[
//           {
//             path:'',
//             loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
//           },
//           {
//             path: 'homeview/:id',
//             children:[
//               {
//                 path:'',
//                 loadChildren: () => import('./homeview/homeview.module').then( m => m.HomeviewPageModule)
//               },
//               {
//                 path: 'bookview/:id',
//                 loadChildren: () => import('./bookview/bookview.module').then( m => m.BookviewPageModule)
//               },
//             ]
//           },
         
//         ] 
//       },

      
//     {
//         path: 'notifications',
//         children :[
//           {
//             path:'',
//             loadChildren: () => import('./notifications/notifications.module').then( m => m.NotificationsPageModule)
//           }
//         ]
//       },
//       {
//         path: 'feed',
//         children:[
//           {
//             path :'',
//             loadChildren: () => import('./feed/feed.module').then( m => m.FeedPageModule)
//           }
//         ]
//       }
//     ]
//   },

//   {
//     path:'',
//     redirectTo:'tabs/home',
//     pathMatch:'full'    
//   },

 
  


// const routes: Routes = [
//   {
//     path: 'tabs',
//     component: SidehomePage,
//     children :[
//       {
//         path: 'home',
//         children :[
//           {
//             path:'',
//             loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
//           },
//           {
//             path: 'homeview/:id',
//             loadChildren: () => import('./homeview/homeview.module').then( m => m.HomeviewPageModule)
//           }, 
//         ] 
//       },
//       {
//           path: 'notifications',
//           children :[
//             {
//               path:'',
//               loadChildren: () => import('./notifications/notifications.module').then( m => m.NotificationsPageModule)
//             }
//           ]
//       },
//       {
//           path: 'feed',
//           children:[
//             {
//               path :'',
//               loadChildren: () => import('./feed/feed.module').then( m => m.FeedPageModule)
//             }
//           ]
//       },
//       {
//         path: 'bookview/:id',
//         loadChildren: () => import('./bookview/bookview.module').then( m => m.BookviewPageModule)
//       },
//     ]
//   },

//   {
//     path:'',
//     redirectTo:'tabs/home',
//     pathMatch:'full'    
//   },

 
  
// ];