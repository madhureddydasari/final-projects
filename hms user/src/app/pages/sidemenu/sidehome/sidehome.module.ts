import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SidehomePageRoutingModule } from './sidehome-routing.module';

import { SidehomePage } from './sidehome.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SidehomePageRoutingModule
  ],
  declarations: [SidehomePage]
})
export class SidehomePageModule {}
