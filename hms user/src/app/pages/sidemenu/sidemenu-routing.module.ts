import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SidemenuPage } from './sidemenu.page';

const routes: Routes = [
  {
    path: '',
    component: SidemenuPage,
    children:[
      {
        path: 'sidehome',
        loadChildren: () => import('./sidehome/sidehome.module').then( m => m.SidehomePageModule)
      },


      // {
      //   path: 'viewprofile',
      //   loadChildren: () => import('./viewprofile/viewprofile.module').then( m => m.ViewprofilePageModule)
      // },

      // {
      //   path: 'help',
      //   loadChildren: () => import('./help/help.module').then( m => m.HelpPageModule)
      // },
      // {
      //   path: 'about',
      //   loadChildren: () => import('./about/about.module').then( m => m.AboutPageModule)
      // },
      // {
      //   path: 'history',
      //   loadChildren: () => import('./history/history.module').then( m => m.HistoryPageModule)
      // }
    
    ]
  }
 


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SidemenuPageRoutingModule {}
