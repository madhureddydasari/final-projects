import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ViewprofilePage } from './viewprofile.page';

const routes: Routes = [
  {
    path: '',
    component: ViewprofilePage
  },
  {
    path: 'updateprofile/:id',
    loadChildren: () => import('./updateprofile/updateprofile.module').then( m => m.UpdateprofilePageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ViewprofilePageRoutingModule {}
