import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { PatientService } from 'src/app/services/patientservice/patient.service';
import { userStruct } from 'src/app/modal/patient';
import { Observable } from 'rxjs';
import { NavController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-viewprofile',
  templateUrl: './viewprofile.page.html',
  styleUrls: ['./viewprofile.page.scss'],
})
export class ViewprofilePage implements OnInit {
  patientOnesdb: userStruct;
 
  patientsdb : Observable<userStruct[]>

  constructor(private activatedRoute : ActivatedRoute ,private navCtrl : NavController,private Aauth : AngularFireAuth , private fbservicepatient : PatientService) 
  { 

    let id=this.activatedRoute.snapshot.paramMap.get('id')
    if(id){
      this.patientsdb =this.fbservicepatient.getPatients(id)
      // this.fbservicepatient.getPatients1(id).subscribe(x => {this.patientOnesdb = x[0]})
    }

  }

  ngOnInit() {
    // let userId = this.Aauth.auth.currentUser.uid;
    // console.log(userId)
    // let id=this.activateRoute.snapshot.paramMap.get('id')
    // if(id){
    //   this.fbservicepatient.getPatient(id).subscribe((docdata)=> {
    //     this.patient = docdata
    //   })
    // }

  }

  goback(){ 
    this.navCtrl.navigateRoot('/sidemenu/sidehome')
  }
 
}
