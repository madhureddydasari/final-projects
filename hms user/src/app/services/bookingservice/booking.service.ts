import { Injectable } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestore , DocumentReference } from '@angular/fire/firestore';
import { bookingstruct } from 'src/app/modal/Doctor';
import { userStruct } from "src/app/modal/patient";
import { Observable } from 'rxjs';
import {  map, take} from 'rxjs/operators';
import { AngularFireAuth } from '@angular/fire/auth';
// import { AngularFireDatabase } from "@angular/fire/database";
// import { promise, $ } from 'protractor';
import { ToastController, NavController, AlertController } from '@ionic/angular';
import { PatientService } from '../patientservice/patient.service';
import { BookviewPage } from 'src/app/pages/sidemenu/sidehome/bookview/bookview.page';


@Injectable({
  providedIn: 'root'
})

export class BookingService {
  
  bookingsdb : Observable<bookingstruct[]>
  patientsdb : Observable<userStruct[]>

  bookingCollection : AngularFirestoreCollection<bookingstruct>
  patientsCollection : AngularFirestoreCollection<userStruct>
  // patientsdb1: userStruct;


  constructor(private alertCtrl : AlertController,private fbservicepatient : PatientService,private navCtrl : NavController,private toastCtrl : ToastController, private afs : AngularFirestore , private Aauth : AngularFireAuth) 
  { 
          // this.bookingsdb = this.bookingCollection
          // .snapshotChanges().pipe(
          //   map(actions =>{
          //     return actions.map(a=> {
          //       const data = a.payload.doc.data();
          //       const bid = a.payload.doc.id;
          //       return { bid, ...data };
          //     });
          //   })
          // )

          // try{
          //   let uid = this.Aauth.auth.currentUser.uid 
          //   // this.patientsdb=this.fbservicepatient.getPatients1(uid);
          //   this.fbservicepatient.getPatients1(uid).subscribe((patientsdb)=>{ this.patientsdb1=patientsdb[0] })
            
          // }

  }

//  this is to get details of a single doctor , similarly do for single particular booking
          // getDoctor(id: string) : Observable<Doctorstruct>{
          //   return this.DoctorCollection.doc<Doctorstruct>(id).valueChanges().pipe(
          //     take(1),
          //     map(firestoreDocumentValue => {
          //       firestoreDocumentValue.id = id;
          //       return firestoreDocumentValue;
          //     })
          //   );
          // }


          //In history page to load all bookings of current user 
  getBookings(currentuseruid) : Observable<bookingstruct[]>
  {
            // define data base in collection variable i.e. stores collection name for suppose
                // to find current user and get details ie. bookings of only that user based on userId of login user122222113333333333 ref => ref.where("toInclude","==",true) && ref.where("doctorExist","==",true)
                // and the userUID (i.e. uid of signedinuser )that is stored while creating
        this.bookingCollection = this.afs.collection<bookingstruct>('BookingsList' , ref => ref.where('userUID','==',currentuseruid).orderBy('bookingDate','desc')) ;
// this is to be prsent in constructor but we place here because it does load before the id retrieved from firebase
        this.bookingsdb = this.bookingCollection
        .snapshotChanges().pipe(
          map(actions =>{
            return actions.map(a=> {
              const data = a.payload.doc.data();
              const id = a.payload.doc.id;
              return { id, ...data };
            });
          })
        ) 
        return this.bookingsdb;
 
  }

  // in bookview to load only current user bookings with the current doctor
  getAllBookingsInParticular(currentuseruid,selectedDoctorId) : Observable<bookingstruct[]>
  {
            // define data base in collection variable i.e. stores collection name for suppose
                // to find current user and get details ie. bookings of only that user based on userId of login user122222113333333333 ref => ref.where("toInclude","==",true) && ref.where("doctorExist","==",true)
                // and the userUID (i.e. uid of signedinuser )that is stored while creating
        this.bookingCollection = this.afs.collection<bookingstruct>('BookingsList' , ref => ref.where('userUID','==',currentuseruid).where('doctorID','==',selectedDoctorId).orderBy('bookingDate','desc')) ;
// this is to be prsent in constructor but we place here because it does load before the id retrieved from firebase
        this.bookingsdb = this.bookingCollection
        .snapshotChanges().pipe(
          map(actions =>{
            return actions.map(a=> {
              const data = a.payload.doc.data();
              const id = a.payload.doc.id;
              return { id, ...data };
            });
          })
        ) 
        return this.bookingsdb;
  }
  

  getbooking(id: string) : Observable<bookingstruct>{
    return this.bookingCollection.doc<bookingstruct>(id).valueChanges().pipe(
      take(1),
      map(firestoreDocumentValue => {
        firestoreDocumentValue.id = id;
        return firestoreDocumentValue;
      })
    );
  }



  bookDoctor(book : bookingstruct , patientInfo : userStruct) 
  {
    try{
            let uid = this.Aauth.auth.currentUser.uid ;
            // this.patientsdb=this.fbservicepatient.getPatients1(uid);
            // this.fbservicepatient.getPatients1(uid).subscribe((patientsdb)=>{ this.patientsdb1=patientsdb[0] });
            book.userUID = uid ;
            // console.log(this.patientsdb1);
            // book.userFullName = this.patientsdb1.userFullName;
            // book.userPatientListDocId = this.patientsdb1.id;

            return this.bookingCollection.add(book)
            .then(ref => {
              ref.set({ bookingId : ref.id},{merge : true})
              // merege says that add field bookingId and everything else is same as before
              .then(() => {
                this.showToast("booking id is :"+ ref.id)
                // console.log("add aextra field also")
              })
            })
          
    }
          catch {
            // this.showToast("booking not confirmed please try Again ")
            this.showAlert("sry For Inconvenience","booking not confirmed please try Again", " refused due to some nentwork issues")
            patientInfo.limit--
          } 
  }

    showToast(message :string) {
      this.toastCtrl.create({
        message ,
        duration : 2000
      }).then((toast) => {
        toast.present()
      })
    }

    showAlert(header:string ,subHeader : string, message:string ,)
    {
      this.alertCtrl.create({
        header,
        subHeader,
        message,
        buttons:["OK"]
      }).then((alert) => {
        alert.present()
      })

    }
}
 