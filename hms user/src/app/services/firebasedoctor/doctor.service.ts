import { Injectable } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestore , DocumentReference } from '@angular/fire/firestore';
import { Doctorstruct} from 'src/app/modal/Doctor';
import { Observable } from 'rxjs';
import {  map, take} from 'rxjs/operators';
import { AngularFireAuth } from '@angular/fire/auth';
import { ToastController, NavController } from '@ionic/angular';

 
@Injectable({
  providedIn: 'root'
})
export class DoctorService {

  private doctorsdb: Observable<Doctorstruct[]>
  private DoctorCollection: AngularFirestoreCollection<Doctorstruct>;
 


  constructor(private navCtrl : NavController,private toastCtrl : ToastController, private afs : AngularFirestore , private Aauth : AngularFireAuth) 
  {


    this.DoctorCollection = this.afs.collection<Doctorstruct>('DoctorsList',ref => ref.where("toInclude","==",true).where("doctorExist","==",true));

    this.doctorsdb = this.DoctorCollection.snapshotChanges().pipe(
      map(actions =>{
        return actions.map(a=> {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })

    ) 
  }

  getAllDoctors() : Observable<Doctorstruct[]>{
    return this.doctorsdb;
  }
  
  getDoctor(id: string) : Observable<Doctorstruct>{
    return this.DoctorCollection.doc<Doctorstruct>(id).valueChanges().pipe(
      take(1),
      map(firestoreDocumentValue => {
        firestoreDocumentValue.id = id;
        return firestoreDocumentValue;
      })
    );
  } 

}



