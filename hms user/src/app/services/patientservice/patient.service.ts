import { Injectable } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestore , DocumentReference } from '@angular/fire/firestore';
import { userStruct} from '../../modal/patient';
import { Observable } from 'rxjs';
import {  map, take} from 'rxjs/operators';
import { AngularFireAuth } from '@angular/fire/auth';
// import { AngularFireDatabase } from "@angular/fire/database";
// import { promise, $ } from 'protractor';
import { ToastController, NavController } from '@ionic/angular';
import * as x from 'rxjs/add/operator/mergeMap';


@Injectable({
  providedIn: 'root'
})
export class PatientService {


  patientsdb : Observable<userStruct[]>
  patientssCollection : AngularFirestoreCollection<userStruct>

  patientsdb1 : Observable<userStruct[]>
  patientssCollection1 : AngularFirestoreCollection<userStruct>


  constructor(private navCtrl : NavController,private toastCtrl : ToastController, private afs : AngularFirestore , private Aauth : AngularFireAuth) 
  { 
    this.patientssCollection = this.afs.collection<userStruct>('PatientsList');
  }
 

// rettuens current patient details of allpatients
  getPatients(currentUserId) : Observable<userStruct[]> 
  {
    console.log("from patient service "+currentUserId)
    this.patientssCollection = this.afs.collection<userStruct>('PatientsList' , ref => ref.where("userUID","==",currentUserId));
    this.patientsdb = this.patientssCollection.snapshotChanges().pipe(
      map(actions =>{
        return actions.map(a=> {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id; 
          return { id, ...data };
        });
      })
    )
    return this.patientsdb
  }


  // getPatients1 method to say that this is being used in another service that is booking service , its not mandatory ,
  // but i have created for my understanding
  getPatients1(currentUserId) : Observable<userStruct[]> 
  {
    console.log("from patient service "+currentUserId)
    this.patientssCollection1 = this.afs.collection<userStruct>('PatientsList' , ref => ref.where("userUID","==",currentUserId));
    this.patientsdb1 = this.patientssCollection.snapshotChanges().pipe(
      map(actions =>{
        return actions.map(a=> {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id; 
          return { id, ...data };
        });
      })
    )
    return this.patientsdb1
    
    // .subscribe(patientsdb => { this.patientonedb = patientsdb[0]})

// return this.patientsdb.subscribe(patientsdb => { this.patientonedb = patientsdb[0]})

  }

  // opportunities = patientsdb1
  // opportunitiesCollection = patientsCollection
// TO get the first one
// return this.patientsdb.subscribe(patientsdb => { this.patientonedb = patientsdb[0]})

// get details of any one patient using id of document
  getPatient(id: string) : Observable<userStruct>{
    return this.patientssCollection.doc<userStruct>(id).valueChanges().pipe(
      take(1),
      map(firestoreDocumentValue => {
        firestoreDocumentValue.id = id;
        return firestoreDocumentValue;
      })
    );
  }

  // updateDoctor(doctor: userStruct) : Promise<void>
  // return this.patients.doc(doctor.id).update({
  // }

  // updates the details of the patient
  updatePatient (patient : userStruct) 
  {
    return this.patientssCollection.doc(patient.id)
    .update({
      userFullName : patient.userFullName,
      limit:patient.limit,
      lastBookingDate:patient.lastBookingDate,
      userImageUrl : patient.userImageUrl,
    })
    .then(()=> {
      this.toastCtrl.create({ message : "userProfile successfully updated",duration : 2000})
    .then((toast) => { toast.present()})
    })
    .then(() =>{
      this.navCtrl.back()
    })
  }

  updatePatient1 (patient : userStruct) 
  {
    return this.patientssCollection.doc(patient.id)
    .update({
      userFullName : patient.userFullName,
      limit:patient.limit,
      lastBookingDate:patient.lastBookingDate,
      userImageUrl : patient.userImageUrl,
    })
    .then(()=> {
      this.toastCtrl.create({ message : "userProfile successfully updated",duration : 2000})
    .then((toast) => { toast.present()})
    })
    .then(() =>{
      // this.navCtrl.back()
    })
  }


  updatePatient2 (patient : userStruct , lbd , limit) 
  {
    return this.patientssCollection.doc(patient.id)
    .set({
      lastBookingDate:lbd,
      limit:limit
    },
    {
      merge:true
    })
    .then(()=> {
      this.toastCtrl.create({ message : "userProfile successfully updated",duration : 2000})
    .then((toast) => { toast.present()})
    })
    .then(() =>{
      // this.navCtrl.back()
    })
  }



// addPatient(patientdata : userStruct)  : Promise<DocumentReference>{
    addPatient(patientdata : userStruct) {
    // let id=this.Aauth.auth.currentUser.uid;
    // console.log("logged in user id is "+patientdata.userUID)
    // patientdata.userId = id;
    return this.patientssCollection.add(patientdata)
    .then((res)=>{console.log(res) })
    .then(() => { this.Aauth.auth.signOut() })
    .then(() => {
      this.showToast("user created successfully")
      this.navCtrl.navigateRoot("/index/login"); 
    })
    // .then(()=> {})

  }

  showToast(message :string)
  {
    this.toastCtrl.create({
      message:message,
      duration : 2000
    }).then((toast) => { toast.present()})

  }


}
  